#!/bin/bash

cat odm/lib64/libarcsoft_turbo_hdr_raw_front.so.* 2>/dev/null >> odm/lib64/libarcsoft_turbo_hdr_raw_front.so
rm -f odm/lib64/libarcsoft_turbo_hdr_raw_front.so.* 2>/dev/null
cat odm/lib64/libarcsoft_turbo_fusion_raw_super_night.so.* 2>/dev/null >> odm/lib64/libarcsoft_turbo_fusion_raw_super_night.so
rm -f odm/lib64/libarcsoft_turbo_fusion_raw_super_night.so.* 2>/dev/null
cat odm/lib64/libCipParameter0.so.* 2>/dev/null >> odm/lib64/libCipParameter0.so
rm -f odm/lib64/libCipParameter0.so.* 2>/dev/null
cat odm/lib64/libCipParameter2.so.* 2>/dev/null >> odm/lib64/libCipParameter2.so
rm -f odm/lib64/libCipParameter2.so.* 2>/dev/null
cat odm/lib64/libCipParameter1.so.* 2>/dev/null >> odm/lib64/libCipParameter1.so
rm -f odm/lib64/libCipParameter1.so.* 2>/dev/null
cat odm/lib64/libCipParameter3.so.* 2>/dev/null >> odm/lib64/libCipParameter3.so
rm -f odm/lib64/libCipParameter3.so.* 2>/dev/null
cat odm/lib64/libarcsoft_turbo_hdr_raw.so.* 2>/dev/null >> odm/lib64/libarcsoft_turbo_hdr_raw.so
rm -f odm/lib64/libarcsoft_turbo_hdr_raw.so.* 2>/dev/null
cat bootRE/boot.elf.* 2>/dev/null >> bootRE/boot.elf
rm -f bootRE/boot.elf.* 2>/dev/null
cat system_ext/app/NetworkAssistSys/NetworkAssistSys.apk.* 2>/dev/null >> system_ext/app/NetworkAssistSys/NetworkAssistSys.apk
rm -f system_ext/app/NetworkAssistSys/NetworkAssistSys.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null >> my_product/app/OplusCamera/OplusCamera.apk
rm -f my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/app/StdSP/StdSP.apk.* 2>/dev/null >> my_heytap/app/StdSP/StdSP.apk
rm -f my_heytap/app/StdSP/StdSP.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat my_heytap/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> my_heytap/priv-app/Phonesky/Phonesky.apk
rm -f my_heytap/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat my_stock/app/AIUnit/AIUnit.apk.* 2>/dev/null >> my_stock/app/AIUnit/AIUnit.apk
rm -f my_stock/app/AIUnit/AIUnit.apk.* 2>/dev/null
cat my_stock/del-app/OppoRelax/OppoRelax.apk.* 2>/dev/null >> my_stock/del-app/OppoRelax/OppoRelax.apk
rm -f my_stock/del-app/OppoRelax/OppoRelax.apk.* 2>/dev/null
cat my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null >> my_stock/del-app/OPBreathMode/OPBreathMode.apk
rm -f my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null
cat my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null >> my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk
rm -f my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null
cat my_stock/priv-app/LinktoWindows/LinktoWindows.apk.* 2>/dev/null >> my_stock/priv-app/LinktoWindows/LinktoWindows.apk
rm -f my_stock/priv-app/LinktoWindows/LinktoWindows.apk.* 2>/dev/null
cat my_stock/priv-app/Games/Games.apk.* 2>/dev/null >> my_stock/priv-app/Games/Games.apk
rm -f my_stock/priv-app/Games/Games.apk.* 2>/dev/null
cat vendor_bootimg/03_dtbdump_Qualcomm_Technologies,_Inc._Ukee_SoC.dtb.* 2>/dev/null >> vendor_bootimg/03_dtbdump_Qualcomm_Technologies,_Inc._Ukee_SoC.dtb
rm -f vendor_bootimg/03_dtbdump_Qualcomm_Technologies,_Inc._Ukee_SoC.dtb.* 2>/dev/null
